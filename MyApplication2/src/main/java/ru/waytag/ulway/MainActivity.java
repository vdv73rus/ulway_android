package ru.waytag.ulway;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.koushikdutta.ion.Ion;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends ListActivity implements LocationListener {
    private Double latitude;
    private Double longitude;
    private LocationManager locationManager;
    private String provider;

    // url to make request
    private static String get_tweets_url = "http://waytag.ru/api/cities/ul/tweets.json";

    // contacts JSONArray
    JSONArray contacts = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean enabled = locationManager
                .isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (!enabled) {
            Toast.makeText(this, "Необходимо включить GPS", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);

        // Initialize the location fields
        if (location != null) {
            System.out.println("Provider " + provider + " has been selected.");
            onLocationChanged(location);
        } else {
            latitude = -1.0;
            longitude = -1.0;
        }

        final ListView listview = (ListView) findViewById(android.R.id.list);
    }

    public static JSONObject getJson(String get_tweets_url){

        InputStream is = null;
        String result = "";
        JSONObject jsonObject = null;

        // HTTP
        try {
            HttpClient httpclient = new DefaultHttpClient(); // for port 80 requests!
            HttpGet httppost = new HttpGet(get_tweets_url);

            HttpResponse response = httpclient.execute(httppost);
            HttpEntity entity = response.getEntity();
            is = entity.getContent();
        } catch(Exception e) {
            return null;
        }

        // Read response to string
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is,"utf-8"),8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
            is.close();
            result = sb.toString();
        } catch(Exception e) {
            return null;
        }

        // Convert string to object
        try {
            jsonObject = new JSONObject(result);
        } catch(JSONException e) {
            return null;
        }

        return jsonObject;

        // Initialize the location fields
    }

    /* Request updates at startup */
    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    /* Remove the locationlistener updates when Activity is paused */
    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }

    // Hashmap for ListView
    ArrayList<HashMap<String, String>> tweetList = new ArrayList<HashMap<String, String>>();
    JSONArray tweets = null;


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    public void onBackPressed(View view) {
        tweetList.clear();
        JSONObject json = getJson(get_tweets_url);
        try {
            // Getting Array of Contacts
            tweets = json.getJSONArray("tweets");
            // looping through All Contacts
            for(int i = 0; i < tweets.length(); i++){
                JSONObject c = tweets.getJSONObject(i);
                // Storing each json item in variable
                String text = c.getString("text");
                String mentor = c.getString("mentor");
                String mapUrl = c.getString("map");
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("text", text);


                ImageView ionimage = (ImageView) findViewById(R.id.imageButton);

                if (ionimage != null) {

                    Ion.with(ionimage).placeholder(R.drawable.staticmap)
                        .error(R.drawable.staticmap).load(mapUrl);
                }

                // adding HashList to ArrayList
                tweetList.add(map);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("*************");
        System.out.println(tweetList);
        System.out.println("*************");

        ListAdapter adapter = new SimpleAdapter(this, tweetList,
                R.layout.rowlayout,
                new String[] { "text" }, new int[] {
                R.id.textView});
        setListAdapter(adapter);
    }

    public void onDtpPress(View view) throws Exception {
        post("dtp");
    }

    public void onDpsPress(View view) throws Exception {
        post("dps");
    }

    public void onPrbkPress(View view) throws Exception {
        post("prpk");
    }

    public void onCamPress(View view) throws Exception {
        post("cam");
    }

    private void post(String type) throws Exception {
        _fixLocation();

        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

        String result = "";

        if (networkInfo != null && networkInfo.isConnected()) {

            HttpPoster poster = new HttpPoster();

            if (latitude != -1 && longitude != -1) {
                if (poster.sendPost(type, longitude, latitude)) {
                    result = "Сообщение отправлено";
                } else {
                    result = "Ошибка при отправке";
                }
            } else {
                result = "Невозможно определить положение";
            }
        } else {
            result = "Ошибка при подключении";
        }

        Toast.makeText(this, result, Toast.LENGTH_SHORT).show();
    }

    private void _fixLocation() {
    }

    private class StableArrayAdapter extends ArrayAdapter<String> {

        HashMap<String, Integer> mIdMap = new HashMap<String, Integer>();

        public StableArrayAdapter(Context context, int textViewResourceId,
                                  List<String> objects) {
            super(context, textViewResourceId, objects);
            for (int i = 0; i < objects.size(); ++i) {
                mIdMap.put(objects.get(i), i);
            }
        }

        @Override
        public long getItemId(int position) {
            String item = getItem(position);
            return mIdMap.get(item);
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

    }
}

